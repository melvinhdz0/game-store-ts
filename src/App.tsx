import React from "react";
import NavBar from "./components/NavBar/NavBar";
import HomePage from "./pages/home/home";
import GameList from "./pages/gameList/gameList";
import GameDetail from "./pages/gameDetails/gameDetails";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App(): JSX.Element {
  return (
    <Router>
      <div className="App">
        <NavBar />
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/gameList" component={GameList} />
          <Route path="/gameDetails/:id" component={GameDetail} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
