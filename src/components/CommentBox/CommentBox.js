import React from 'react';
import Comments from '../Comments/Comments';
import './CommentBox.css';

const commentBox = (porps)=>{
  const comments = porps.commentArray;
  return(
    <div className="commentBox">
      {comments.length != 0 ?
        comments.map( singleComment =>{
          <Comments author={`${singleComment.user.firstName} ${singleComment.user.lastName}`} commentBody={singleComment.body}/>
        }) : <h3>No coments yet</h3>
      }
    </div>
  );
}

export default commentBox;