import React from "react";
import Labels from "../Label/Label";
import './Comments.css'

const comments = (props) => {
  console.log(props);
  return (
    <div className="commentSingle">
      <Labels title={props.author} />
      <Labels text={props.commentBody} />
    </div>
  );
};

export default comments;