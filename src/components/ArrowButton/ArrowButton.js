import React from 'react';
import './ArrowButton.css';

const arrowPlace = ()=>{
  return (
    <div className="arrow">
      <i className="fas fa-greater-than"></i>
    </div>
  );
}

export default arrowPlace;