import React from "react";
import MenuItems from "../MenuItems/MenuItems";
import "./NavBar.css";
import { Link } from "react-router-dom";

const navBar = () => {
  function displayMenu() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
      x.className += " responsive";
    } else {
      x.className = "topnav";
    }
  }

  return (
    <div className="topnav" id="myTopnav">
      <Link to="/">
        <MenuItems text="Home" />
      </Link>
      <Link to="/gameList">
        <MenuItems text="Games List" />
      </Link>
      <a href="javascript:void(0);" className="icon" onClick={displayMenu}>
        <i className="fas fa-bars"></i>
      </a>
    </div>
  );
};

export default navBar;
