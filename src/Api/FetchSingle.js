const getDataApiSingle = async (setState, id) =>{
  
  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  const fetchData = await fetch(
    `https://trainee-gamerbox.herokuapp.com/games/${id}`,
    requestOptions)
  
    const ApiData = await fetchData.json();

    setState(ApiData);

}

const getDataComments = async (setState, id) =>{
  
  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  const fetchData = await fetch(
    `https://trainee-gamerbox.herokuapp.com/games/${id}/comments`,
    requestOptions)
  
    const ApiData = await fetchData.json();

    setState(ApiData);

}

export default getDataApiSingle;
export {getDataComments}