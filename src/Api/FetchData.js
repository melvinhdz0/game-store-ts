const getDataApi = async (setState) =>{
  
  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  const fetchData = await fetch(
    `https://trainee-gamerbox.herokuapp.com/games?_limit=8`,
    requestOptions)
  
    const ApiData = await fetchData.json();

    setState(ApiData);

}

export default getDataApi;

const getDataApiPage = async (setState, page) =>{
  
  const requestOptions = {
    method: "GET",
    redirect: "follow",
  };

  const fetchData = await fetch(
    `https://trainee-gamerbox.herokuapp.com/games?_sort=id&_start=${page}`,
    requestOptions)
  
    const ApiData = await fetchData.json();

    setState(ApiData);

}

export function useLocalStorage(page){
  // eslint-disable-next-line no-undef
  const localStore = window.localStorage;
  localStore.setItem('pageNumber',page);
}



export {getDataApiPage};