import { type } from "os";
import React, { useEffect, useState } from "react";
import "./gameDetails.css";
import getDataApiSingle, { getDataComments } from "../../Api/FetchSingle";
import Banner from "../../components/BannerMain/Banner";
import CommentBox from "../../components/CommentBox/CommentBox";


type propsType = {
  match: {
    params: {
      id: string;
    };
  };
};

interface IGame {
  comments: Array<unknown>;
  cover_art: { url: string };
  created_at: string;
  genre: unknown;
  id: number;
  name: string;
  platforms: Array<unknown>;
  price: string;
  publishers: Array<unknown>;
  release_year: number;
  updated_at: string;
}

type Icomments = Array<{
  body: string;
  created_at: string;
  game: unknown;
  id: number;
  trainee: null;
  updated_at: string;
  user: {
    blocked: boolean;
    confirmed: boolean;
    created_at: string;
    email: string;
    firstName: string;
    id: number;
    lastName: string;
    provider: string;
    role: number;
    updated_at: string;
    username: string;
  };
}>;

const GameDetails = (props: propsType): JSX.Element => {
  const [gameSingleState, setGameSingle] = useState<IGame>({
    comments: [],
    cover_art: { url: "" },
    created_at: "",
    genre: {},
    id: 0,
    name: "",
    platforms: [],
    price: "0",
    publishers: [],
    release_year: 0,
    updated_at: "",
  });

  const [commentsState, setComments] = useState<Icomments>([]);

  useEffect(() => {
    getDataApiSingle(setGameSingle, props.match.params.id);
    getDataComments(setComments, props.match.params.id);
  }, []);

  return (
    <div>
      {gameSingleState.id != 0 ? (
        <div className="body-detail">
          <h1>{gameSingleState.name}</h1>
          <Banner
            urlImg={
              gameSingleState.cover_art != null
                ? gameSingleState.cover_art.url
                : ""
            }
          />
          <h3>{gameSingleState.price}$</h3>

          <CommentBox commentArray={commentsState.length != 0 ? commentsState : []}></CommentBox>
        </div>
      ) : (
        "loading"
      )}

     
    </div>
  );
};

export default GameDetails;
