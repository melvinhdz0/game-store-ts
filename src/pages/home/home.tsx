import React, { useState, useEffect, useRef } from "react";
import "./home.css";
import Banner from "../../components/BannerMain/Banner";
import Cards from "../../components/Cards/Cards";
import Footer from "../../components/Footer/Footer";
import getDataApi from '../../Api/FetchData';

const HomePage = ():JSX.Element => {

  const [gameState, setGames] = useState(
    [{
    id:'',
    name: '',
    price:  '',
    cover_art: null
    
    }
  ] 
  );

  useEffect(() => {
    getDataApi(setGames);
  }, []);

  return (
    <div className="pageContent">

      <div className="bannerContent">
        <Banner urlImg="https://previews.123rf.com/images/redlinevector/redlinevector1903/redlinevector190300644/119546618-video-games-neon-text-and-gamepad-with-lightnings-video-game-and-entertainment-design-night-bright-n.jpg" />
      </div>
      <div className="body">
        {gameState.length > 0 ? gameState.map((element) => {
          return(<Cards
            key={element.id}
            title={element.name}
            price={element.price}
            img={element.cover_art}
          ></Cards>
          ) 
        }): <Cards
        key="0"
        title="Loading..."
        price="loading..."
        img={null}
      ></Cards>}
      </div>
      <Footer />
    </div>
  );
};

export default HomePage;
