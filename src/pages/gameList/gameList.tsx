import React, {useRef, useEffect, useState} from "react";
import "./gameList.css";
import Cards from "../../components/Cards/Cards";
import Footer from "../../components/Footer/Footer";
import {getDataApiPage, useLocalStorage} from '../../Api/FetchData';
import { Link } from "react-router-dom";

type IGame = Array<
    {
    comments: Array<unknown>;
    cover_art: { url: string };
    created_at: string;
    genre: unknown;
    id: number;
    name: string;
    platforms: Array<unknown>;
    price: string;
    publishers: Array<unknown>;
    release_year: number;
    updated_at: string;
    }
    
  > 


const GamesList = ():JSX.Element => {
  const pageCount = useRef(0);
  let statusReload = true;
  const [gameListState, setGamesList] = useState<IGame>([]);

  useEffect(() => {
    getDataApiPage(setGamesList,pageCount.current);
  }, []);

  useEffect(() => {
    getDataApiPage(setGamesList,pageCount.current);
  }, [statusReload]);

  function movePageNext(){
    pageCount.current +=8;
    statusReload = !statusReload;
  }
  function movePageBack(){
    pageCount.current -=8;
    statusReload = !statusReload;
  }

  useLocalStorage(pageCount.current);

  return (
    <div className="pageContent">
      <div className="body-list">
      <div className="left-side"></div>
      <div className="middle-side">
        {gameListState.length != 0 ? gameListState.map(valor => {
          return (<Link  key={valor.id} to={`gameDetails/${valor.id}`}><Cards title = {valor.name} price = {valor.price} img = {valor.cover_art} ></Cards></Link>)
        }): <h3>Loading Games</h3>}
        <button  onClick={pageCount.current == 0 ? ()=>{console.log(pageCount.current)}: movePageBack}>{"<"}</button>
        <button  onClick={gameListState.length == 0 ? ()=>{console.log(pageCount.current)} : movePageNext}>{">"}</button>
      </div>
      <div className="right-side"></div>
  </div>
      <Footer />
    </div>
  );
};

export default GamesList;
